var express=require('express');
var path=require('path');
var index=require('./routes/index')
var bodyparser=require('body-parser');
var cookie=require('cookie-parser');
var server=require('./routes/server');
var article=require('./routes/article');
var news=require('./routes/news');
var app=express();

app.set('views',path.join(__dirname,'/views'));
app.set('routes',path.join(__dirname,'/routes'));
app.set('accounts',path.join(__dirname,'/acocunts'));
app.set('view engine','jade');
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:true}))
var env=app.get('env')
if (env=='development'){
  app.locals.pretty = true; 
}
//app.static() : 外部ファイルで、相対パスを使えるようにするための関数（重要）

app.use('/statics',express.static(path.join(__dirname,'statics')));
app.use('/views',express.static(path.join(__dirname,'views')));
//フロントエンドのプログラミング
app.use('/',index);
//サーバーサイドのプログラミング

app.use('/server',server);
app.use('/sever2',article);
app.use('/server3',news);


app.listen(4200,function(){
  console.log('The server is running at http://127.0.0.1:4200')
});