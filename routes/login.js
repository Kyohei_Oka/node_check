var router=require('express').Router;
var passport=require('passport');
var strategy=require('passport-local').Strategy;
var sql=require('mysql');


router.use(passport.initialize());
router.use(passport.session());

router.get('/login',passport.authenticate(),function(req,res){
    res.redirect('/');
})

router.post('/logout',function(req,res){
    req.logout();
    res.redirect('/');
})

exports.module=router;

