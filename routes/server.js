var router=require('express').Router();
var db=require('./db');

var name='accounts';

router.get('/',db.db_get(name));
router.get('/:id',db.db_detail(name));
router.post('/',db.db_post(name));
router.put('/:id',db.db_update(name));
router.delete('/:id',db.db_delete(name));

module.exports=router;