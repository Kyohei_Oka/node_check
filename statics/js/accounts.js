

angular.module('App',['ngResource','ngRoute'])
.config(['$routeProvider','$locationProvider',function($routeProvider,$locationProvider){
    $locationProvider.html5Mode(true);
    $routeProvider
    .when('/',{
        templateUrl:'views/main/index.html',
    })
    .when('/DENX',{
        templateUrl:'views/main/denx.html'
    })
    .when('/schedule',{
        template:'<h1>行事予定</h1>'
    })
    .when('/activities',{
        template:'<h1>活動報告</h1>'
    })
    .when('/access',{
        template:'<h1>お問い合わせ</h1>'
    })
    .when('/accounts',{
        templateUrl:'views/crud/accounts.html',
        controller:'table'
    })
    .when('/accounts/create',{
        templateUrl:'views/crud/create.html',
        controller:'table'
    })

    .otherwise({
        template:'<h1>未実装です。</h1>'
    })
}])
//インデックス側もAjaxを通して記事の取得を行う。

.controller('table',['$scope','$resource',function($scope,$resource){
    var Accounts=$resource('/server/:id',{id:'@id'},{update:{method:'PUT'}});
    $scope.accounts=Accounts.query(function(){
        $scope.accounts.birth=new Date($scope.accounts.birth);
    });
    
        $scope.create=function(){
                $scope.data.birth=new Date($scope.data.birth).toLocaleDateString()
                Accounts.save($scope.data,function(){
                $scope.accounts.birth=new Date($scope.accounts.birth);
                $scope.accounts=Accounts.query();
                    // HTML5 の date 型にバインドするなら、以下のようにDate型にはめて入力               
            })
        }
        //detail 機能は削除することを考慮しておく。
        $scope.ondetail=function(id){
            $scope.updata=Accounts.get({id:id},function(){
                $scope.updata.birth=new Date($scope.updata.birth);
            });
        };
        $scope.onupdate=function(){
            $scope.updata.birth=new Date($scope.updata.birth).toLocaleDateString();
            Accounts.update($scope.updata,function(){ 
                $scope.accounts.birth=new Date($scope.accounts.birth);
                $scope.accounts=Accounts.query();          
                
            })
        }
        
        $scope.ondelete=function(id){
            var ask=confirm('データを削除しますか？');
            if(ask==true){
            Accounts.delete({id:id},function(){
                $scope.accounts=Accounts.query(function(){
                    $scope.accounts.birth=new Date($scope.accounts.birth);

                });
            }
            )};
    };
   }])

   